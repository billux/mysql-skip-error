#!/bin/sh

# File containing error messages to skip (one per line).
error_messages="errors.txt"

# Sleep interval between 2 check.
sleep_interval="1"

# Exit when Seconds_Behind_Master reached 0.
exit_when_uptodate="false"

# Options to pass to mysql.
#mysql_opt="-P 3307"

# File to log skipped queries to (leave empty for no logs).
log_file=""

mysql_skip_error() {
    error="$1"

    printf "Skiping: $error\n"
    mysql $mysql_opt -e 'SET GLOBAL SQL_SLAVE_SKIP_COUNTER=1; START SLAVE;'

    [ -n "$log_file" ] && echo "$error" >>"$log_file"
}

while true; do
    slave_status="$(mysql $mysql_opt -e 'SHOW SLAVE STATUS\G')"
    seconds_behind_master=$(echo "$slave_status" |grep 'Seconds_Behind_Master: ' |awk -F ' ' '{print $2}')
    last_SQL_error="$(echo "$slave_status" |grep 'Last_SQL_Error: ' |sed 's/^.\+Last_SQL_Error: //')"

    if $exit_when_uptodate && [ "$seconds_behind_master" = "0" ]; then
        printf 'Replication is up to date!\n'
        exit 0

    elif [ -z "$last_SQL_error" ]; then
        sleep $sleep_interval

    elif echo "$last_SQL_error" |grep -q -f $error_messages; then
        mysql_skip_error "$last_SQL_error"

    else
        printf "Current SQL error doesn't match the pattern:\n"
        printf "$last_SQL_error\n"
        printf "Skip it? [y/N]: "
        read reply
        if [ "$reply" = "y" ] || [ "$reply" = "Y" ]; then
            mysql_skip_error $last_SQL_error
        fi
    fi
done
